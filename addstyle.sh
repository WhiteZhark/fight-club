#!/bin/bash

line_contents=$(sed -n '12p' Scoreboard.html)
if [[ "$line_contents" == *"LINK"* ]] ; then
    :
else
    sed -i '12 s/.*/    <LINK rel=StyleSheet href="style.css" type="text\/css" media=screen>/' Scoreboard.html
    sed -i '13,17d' Scoreboard.html
    echo "style added to Scoreboard"
fi

line_contents=$(sed -n '12p' Bettors.html)
if [[ "$line_contents" == *"LINK"* ]] ; then
    :
else
    sed -i '12 s/.*/    <LINK rel=StyleSheet href="style.css" type="text\/css" media=screen>/' Bettors.html
    sed -i '13,17d' Bettors.html
    echo "style added to Bettors"
fi

line_contents=$(sed -n '8p' Bettors.html)
if [[ "$line_contents" == *"text\/css"* ]] ; then
    :
else
    sed -i '8 s/.*/ <LINK rel=StyleSheet href="style.css" type="text\/css" media=screen>/' directory.html
    sed -i '9,27d' directory.html
    echo "style added to directory"
fi